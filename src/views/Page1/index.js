// 某个页面的入口文件，一般为路由组件

import React from 'react';

export default class Page1 extends React.Component {
    render() {
        return (
            <section>
              <h1> page1  </h1>
            </section>
        );
    }
}
