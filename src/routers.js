import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

import Template1 from 'src/layout/Page.js'
import Admin from 'src/layout/Admin.js'
import Login from 'views/Login/index.js'

const Routers = () =>(
  <Router>
    <Switch>
      <Route path="/admin" component={Admin} />
      <Route path="/login" component={Login} />
      <Route path="/" component={Template1} />
    </Switch>
  </Router>
)
export default Routers;
