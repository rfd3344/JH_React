//  All reducers
import { combineReducers } from 'redux'

import Todos from 'views/Todos/Redux.js'
import ProductFilter from 'views/Products/Redux.js'
import Quoridor from 'views/Quoridor/Redux.js'

/******************* Action reducer Example  *******************/
export const addTodo = text => ({
  type: 'ADD_TODO',
  text
})
const reducer1 = (state = [], action) => {
  switch (action.type) {
    case 'action1':
      return [
        ...state,
        { text: 'action1 text'}
      ]
    default:
      return state
  }
}
/*********************************/

export default combineReducers({
  reducer1,
  Todos,
  ProductFilter,
  Quoridor
})
