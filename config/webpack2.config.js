const path = require('path');
//  const merge = require('webpack-merge');

// const NODE_ENV = process.env.NODE_ENV = process.env.BABEL_ENV = 'development';

//  const webpackBaseConfig = null
// require('./webpack.conf.base.js')(NODE_ENV);

const appData = require('../data.json');
const seller = appData.seller;
const goods = appData.goods;
const ratings = appData.ratings;


module.exports = {
    entry: path.resolve(__dirname, '../src/index.js'),
    mode: 'production',
    devServer: {
        before(app) {
            app.get('/api/seller', (req, res) => {
                res.json({
                    error: 0,
                    data: appData,
                });
            });
            app.get('/api/goods', (req, res) => {
                res.json({
                    error: 0,
                    data: goods
                });
            });
            app.get('/api/ratings', (req, res) => {
                res.json({
                    error: 0,
                    data: ratings
                });
            });
        },
        // historyApiFallback: true,
        // inline: true,
        // contentBase: path.resolve(__dirname, '../dist/'),
        port: 5000,
        host: '0.0.0.0',
    },
//    devtool: 'source-map'
}
